-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 05, 2016 at 10:11 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomic_project_b36`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE IF NOT EXISTS `birthday` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `birth_date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `birth_date`) VALUES
(1, 'shamon', '1980-11-15'),
(2, 'sikder', '1981-11-21');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE IF NOT EXISTS `book_title` (
`id` int(11) NOT NULL,
  `book_title` varchar(255) NOT NULL,
  `author_name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_title`, `author_name`, `price`) VALUES
(1, 'Pride & prigse', 'sackPer', 2010),
(2, 'shahanama', 'fardochy', 100);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city`) VALUES
(1, 'shamon', 'Dhaka'),
(2, 'sikder', 'Chittagong');

-- --------------------------------------------------------

--
-- Table structure for table ` email`
--

CREATE TABLE IF NOT EXISTS ` email` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table ` email`
--

INSERT INTO ` email` (`id`, `name`, `email`) VALUES
(1, 'shol', 'adsdsdd.fdfds22@yahoo.com'),
(2, 'HANIF', 'DSJHFYT821@GMAIL.COM\r\n');

-- --------------------------------------------------------

--
-- Table structure for table ` gender`
--

CREATE TABLE IF NOT EXISTS ` gender` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  ` gender` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table ` gender`
--

INSERT INTO ` gender` (`id`, `name`, ` gender`) VALUES
(1, 'tAZUL', 'Meal'),
(2, 'DeLOR', 'Meal');

-- --------------------------------------------------------

--
-- Table structure for table ` hobbies`
--

CREATE TABLE IF NOT EXISTS ` hobbies` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  ` hobbies` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table ` hobbies`
--

INSERT INTO ` hobbies` (`id`, `name`, ` hobbies`) VALUES
(1, 'Tuhin', 'Play Football'),
(2, 'Tariq', 'Summing');

-- --------------------------------------------------------

--
-- Table structure for table ` profile_picture`
--

CREATE TABLE IF NOT EXISTS ` profile_picture` (
`id` int(11) NOT NULL,
  `nane` varchar(255) NOT NULL,
  ` profile_picture` varchar(255) NOT NULL,
  `caption` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table ` profile_picture`
--

INSERT INTO ` profile_picture` (`id`, `nane`, ` profile_picture`, `caption`) VALUES
(1, 'Deder', 'image/capture.png', 'sharif'),
(2, 'Rathun', 'image/beach.png', 'dedar');

-- --------------------------------------------------------

--
-- Table structure for table ` summary_of_organization`
--

CREATE TABLE IF NOT EXISTS ` summary_of_organization` (
`id` int(11) NOT NULL,
  `organization_name` varchar(255) NOT NULL,
  `task` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table ` summary_of_organization`
--

INSERT INTO ` summary_of_organization` (`id`, `organization_name`, `task`) VALUES
(1, 'Kafco', 'Make Urea'),
(2, 'DAP', 'Make dyamoniomphosphet');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table ` email`
--
ALTER TABLE ` email`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table ` gender`
--
ALTER TABLE ` gender`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table ` hobbies`
--
ALTER TABLE ` hobbies`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table ` profile_picture`
--
ALTER TABLE ` profile_picture`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table ` summary_of_organization`
--
ALTER TABLE ` summary_of_organization`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table ` email`
--
ALTER TABLE ` email`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table ` gender`
--
ALTER TABLE ` gender`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table ` hobbies`
--
ALTER TABLE ` hobbies`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table ` profile_picture`
--
ALTER TABLE ` profile_picture`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table ` summary_of_organization`
--
ALTER TABLE ` summary_of_organization`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
